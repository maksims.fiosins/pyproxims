# PyProxyMS python library

## Abot the library
The library is designed to process Mass Spectrometry (proteomics) data from proximity labelling...

## Data format
Proximity labelling experiment should contain 3 groups of samples:
- POI proximity labeling,
- negative control (the cells not transfected by the proximity labeling enzyme),
- positive control (non-specific (cytosolic) proximity labeling enzyme).

This should be provided in a form of abundance matrix with rows corresponding to proteins and columns corresponding to samples.

## Pipeline steps
The main steps of the pipeline are:
- Replicate merge
- Normalization
- Imputation
- PCA plot
- Differential abundance for filtering
- Differential abundance
- SynGO annotation
- Excel report

![pipeline flowchart](docs/proteomics_pipeline_diagram.jpg "Pipeline flowchart")

## Examples

## Shiny server

To process data interactively, the shiny server is included.
