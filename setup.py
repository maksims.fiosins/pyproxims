import setuptools

setuptools.setup(
    name="pyproxims",
    author="UKE ZMMH",
    version="0.0.1",
    author_email="maksims.fiosins@zmnh.uni-hamburg.de",
    description="Python package to analyse proteomics (mass spectrometry) proximity data.",
    packages = ["pyproxims"],
    package_data = {"pyproxims": ["source.functions.r", "test_data/*"]},
    include_package_data=True,
    install_requires=[
        "pandas",
        "matplotlib",
        "shiny",
        "scikit-learn",
        "plotly",
        "rpy2",
        "gseapy",
        "openpyxl",
        "faicons",
        "bioservices"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)

