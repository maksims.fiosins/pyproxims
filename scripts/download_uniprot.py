from pyproxims.proteins import uniprot_query_ids
import pandas as pd
import os

script_folder = os.path.dirname(os.path.abspath(__file__))

res_df = pd.read_excel(os.path.join(script_folder,"../pyproxims/test_data/results_summary_Syn1_BIC.xlsx"))
prot_list = res_df["accession"]

res = uniprot_query_ids(prot_list)
print(res)

