import os

os.environ["R_LIBS"] = "/usr/local/lib/R/site-library/"
#os.environ["R_HOME"] = "C:\Program Files\R\R-4.4.0"
os.environ["R_HOME"] = "/usr/lib/R/"

from shiny import App, Inputs, Outputs, Session, reactive, render, req, ui
import asyncio
from pathlib import Path

import pandas as pd
import numpy as np
from faicons import icon_svg

from pyproxims.imputation import imputation
from pyproxims.normalization import merge_replicates, normalize
from pyproxims.pca import pca_all, pca_plot_all_matplotlib
from pyproxims.diff_abundance import diff_abundance, volcano_plot_matplotlib
from pyproxims.ontology_annotation import calc_goterms, add_in_syngo, calc_enrichment
from pyproxims.proteins import uniprot_query_ids

import matplotlib.pyplot as plt
import math

class SettingsClass:
    _s = {}

    def set_val(self, name, value):
#        print(f"Set {name} = {value}")
        if name in self._s:
            self._s[name].set(value)
        else:
            self._s[name] = reactive.value(value)

    def get_val(self, name, default = None):
        if name not in self._s:
            self._s[name] = reactive.value(default)
        return self._s[name].get()

    def update_protein_sheets(self):
        xl = pd.ExcelFile(self.get_protein_file_path())
        self.set_val("protein_file_sheets", xl.sheet_names)
        self.set_protein_active_sheet(xl.sheet_names[0])

    def get_protein_sheets(self):
        return self.get_val("protein_file_sheets")

    def get_protein_active_sheet(self):
        return self.get_val("protein_active_sheet")

    def set_protein_active_sheet(self, sheet_name):
        print("Setting active sheet")
        self.set_val("protein_active_sheet", sheet_name)
        self.update_protein_data()

    def get_protein_file_path(self):
        return self.get_val("protein_file_path")

    def set_protein_file_path(self, path):
        print("*********************SET FILE PATH")
        if path == self.get_protein_file_path():
            return
        self.set_val("protein_file_path", path)
        self.update_protein_sheets() 
        self.update_protein_data()

    def get_protein_id_column(self):
        return self.get_val("protein_id_column")

    def set_protein_id_column(self, value):
        self.set_val("protein_id_column", value)   

    def set_protein_id_column_default(self):
        text_cols = self.get_protein_text_cols()
        print(f"**************Text cols: {text_cols}")
        cur_protein_id_column = self.get_protein_id_column()
        if cur_protein_id_column in text_cols:
            return
        if len(text_cols)>0:
            self.set_protein_id_column(text_cols[0])
        else:
            self.set_protein_id_column("")

    def get_protein_name_column(self):
        return self.get_val("protein_name_column")

    def set_protein_name_column(self, value):
        self.set_val("protein_name_column", value)   

    def set_protein_name_column_default(self):
        text_cols = self.get_protein_text_cols()
        cur_protein_name_column = self.get_protein_name_column()
        if cur_protein_name_column in text_cols:
            return
        if len(text_cols)>1:
            self.set_protein_name_column(text_cols[1])
        else:
            self.set_protein_name_column("")

    def update_protein_num_cols(self, data):
        num_cols = data.select_dtypes(include=np.number).columns.tolist()
        self.set_val("protein_num_cols", num_cols)
        self.set_val("protein_data_cols", num_cols.copy())

    def update_protein_text_cols(self, data):
        text_cols = data.select_dtypes(exclude=np.number).columns.tolist()
        print(f"**************Text cols A: {text_cols}")
        self.set_val("protein_text_cols", text_cols)
        print(f"**************Text cols B: {text_cols}")
        self.set_protein_id_column_default()
        self.set_protein_name_column_default()

    def get_protein_num_cols(self):
        return self.get_val("protein_num_cols", [])

    def get_protein_data_cols(self):
        return self.get_val("protein_data_cols", [])

    def set_protein_data_cols(self, value):
        self.set_val("protein_data_cols", value)   
    
    def get_protein_text_cols(self):
        return self.get_val("protein_text_cols", [])

    def get_protein_data(self):
        return self.get_val("protein_data")

    def get_protein_display_data(self):
        cur_data = self.get_protein_data()
        if cur_data is None:
            return None
        cur_data = cur_data.copy()
        id_col = self.get_protein_id_column()
        col_list = [id_col]
        name_col = self.get_protein_name_column()
        if name_col != "":
            col_list += [name_col]

        col_list += self.get_protein_data_cols()
        cur_data = cur_data[col_list]

        cur_data.rename(columns={id_col:"Protein ID"}, inplace=True)
        if id_col!=name_col:
             cur_data.rename(columns={name_col:"Protein Name"},  inplace=True)           
        else: 
             index = [i for i, x in enumerate(cur_data.columns) if x == "Protein ID"][1]
             col_list = list(cur_data.columns)
             col_list[index] = "Protein Name"
             cur_data.columns = col_list

        cur_data.set_index("Protein ID")
        return cur_data

    def update_protein_data(self):
        print("*********************Update data")
        data = pd.read_excel(self.get_protein_file_path(), sheet_name = self.get_protein_active_sheet())
        self.update_protein_text_cols(data)
        self.update_protein_num_cols(data)
        print("Setting protein data")
        self.set_val("protein_data", data)


#abundances_df = pd.DataFrame(data=d)
#protein_name_map = None
#data_df = None

syngo_protein_cc_df = pd.read_csv("syngo_data/syngo_proteins_cc_pre_postsynaptic.csv", sep = "\t")
syngo_ontology_df = pd.read_csv("syngo_data/syngo_ontology_hierarchy.csv", sep = "\t")

app_dir = Path(__file__).parent

protein_map = {}
protein_map_gene = {}
def load_protein_map(protein_id_list):
    global protein_map
    global protein_map_gene

    protein_map_source_field = "Entry"
    protein_map_dest_field = "Entry Name Short"
    protein_map_gene_field = "Gene Names"

    proteins_df = uniprot_query_ids(protein_id_list)
    protein_map = {row[protein_map_source_field]: row[protein_map_dest_field] for index, row in protein_map_df.iterrows()}
    protein_map_gene = {row[protein_map_source_field]: row[protein_map_gene_field] for index, row in protein_map_df.iterrows()}

app_ui = ui.page_navbar(
     footer=ui.div(
      ui.include_css(app_dir / "styles.css"),
      ui.navset_pill_list(
        ui.nav_panel("Original data", 
            ui.row(
                ui.column(
                    8,
                    ui.output_data_frame("render_original_df"),
                    ui.tags.div("Enter abundances"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_file("upload", "Upload Excel file with abundances"),
                        class_="pb-1 mb-3",
                    ),
                    ui.output_ui("data_controls"),
                ),
            ),
         icon=icon_svg("upload")   
       ),
       ui.nav_panel("Sample Names / Conditions", 
            ui.row(
                ui.column(
                    8,
                    # For debugging
                    # ui.output_table("table"),
                    ui.panel_well(
                        ui.input_select("sample_name_separator", "Sample Name Separator", ["_",","]),
                        ui.input_select("sample_condition_elems", "Elements in condition", [1,2,3,4]),
                        ui.input_select("sample_name_elems", "Elements in sample name", [1,2,3,4]),
                    ),
                    class_="order-2 order-sm-1",
                ),
                
                ui.column(
                    4,
                    ui.panel_well(
                        ui.output_data_frame("render_samples_conditions_df"),
                    ),
                ),
            ),
         icon=icon_svg("upload")   
       ),
       ui.nav_panel("Uniprot",
            ui.row(
                ui.column(
                    8,
                    
                    ui.panel_well(
                        ui.input_radio_buttons("input_field_genename_source","Gene Name Source", ["table column", "uniprot"]),

                    ),     
                    ui.panel_conditional("input.input_field_genename_source === 'table column'",
                        ui.input_select("input_gene_name", "Gene Name Column", {"":"", "Gene Name": "Gene Name", "NT_BIC_T1": "NT_BIC_T1"}),
                    ),
                    ui.panel_conditional("input.input_field_genename_source === 'uniprot'",
                        ui.input_action_button("query_uniprot","Query uniprot")
                    ),

                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
#                        ui.input_select("imputation_method", "Imputation method", {"norm": "Normal", "zero": "Zero", "lod": "Lod", "knn": "KNN"}),
#                        ui.output_plot("imputation_pca_plot", height="300px"),
#                        output_widget("imputation_pca_plot"),
                        class_="pb-1 mb-3",
                    ),

                ),
            ),
         icon=icon_svg("fill") 
        ),
       ui.nav_panel("Imputation",
            ui.row(
                ui.column(
                    8,
                    ui.output_data_frame("render_imputed_df"),
                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_select("imputation_method", "Imputation method", {"norm": "Normal", "zero": "Zero", "lod": "Lod", "knn": "KNN"}),
                        ui.output_plot("imputation_pca_plot", height="300px"),
#                        output_widget("imputation_pca_plot"),
                        class_="pb-1 mb-3",
                    ),

                ),
            ),
         icon=icon_svg("fill") 
        ),
        ui.nav_panel("Replicate merge",
            ui.row(
                ui.column(
                    8,
                    ui.output_data_frame("render_merged_df"),
                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_text("repl_merge_suffix", "Imputation suffix", "\_T\d+"),
                        ui.output_plot("repl_merge_pca_plot", height="300px"),
                        class_="pb-1 mb-3",
                    ),
                ),

            ),
           icon=icon_svg("fill")   
        ),
        ui.nav_panel("Normalization",
            ui.row(
                ui.column(
                    8,
                    ui.output_data_frame("render_norm_df"),
                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_select("normalization_method", "Normalization method", {"norm": "Normal"}),
                        ui.output_plot("normalization_pca_plot", height="300px"),
                        class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("ruler-combined") 
        ),
        ui.nav_panel("Specificity filtering",
            ui.row(
                ui.column(
                    8,
                    ui.navset_tab(
                         ui.nav("Results",
                                 ui.output_data_frame("render_specificity_df")
                               ),
                         ui.nav("Volcano plot",
                                 ui.output_plot("render_specificity_volcano", height="600px"),
                               ),
                    ),
                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_select("specificity_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
                        ui.input_selectize("specificity_filter_base","Choose base samples:",{}, multiple=True),
                        ui.input_selectize("specificity_filter_spec","Choose specific samples:",{}, multiple=True),
                        ui.input_action_button("specificity_filter", "Filter", class_="btn-success"),
                        ui.input_numeric("spec_logfc_thr", "LogFC threshold", value=1.3, step=0.1,min=0),
                        ui.input_numeric("spec_pval_thr", "pvalue threshold", value=1.3, step=0.1, min=0),
                        ui.input_checkbox("spec_show_filt", "show only filtered"),
                        ui.input_select("spec_pval", "Pvalue", {"p.ord": "p.ord", "p.mod": "p.mod"}),
                        class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("filter") 
        ),
        ui.nav_panel("Specificity SynGO terms",
            ui.row(
                ui.column(
                    8,
                    ui.output_plot("render_specificity_syngo_plot"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
#                         ui.input_select("spec_syngo_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
#                         ui.input_action_button("specificity_syngo_filter", "Filter", class_="btn-success"),
                         class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("brain") 
        ),
        ui.nav_panel("Specificity enrichment",
            ui.row(
                ui.column(
                    8,
                    ui.output_plot("render_specificity_enrich_plot", height="600px"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
#                         ui.input_select("spec_enrichment_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
#                         ui.input_action_button("specificity_enrichment_filter", "Filter", class_="btn-success"),
                         class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("database") 
        ),
        ui.nav_panel("Proximity filtering",
            ui.row(
                ui.column(
                    8,
                    ui.navset_tab(
                         ui.nav("Results",
                                 ui.output_data_frame("render_proximity_df")
                               ),
                         ui.nav("Volcano plot",
                                 ui.output_plot("render_proximity_volcano", height="600px"),
                               ),
                    ),

                    # For debugging
                    # ui.output_table("table"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
                        ui.input_select("proximity_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
                        ui.input_selectize("proximity_filter_base","Choose base samples:",{}, multiple=True),
                        ui.input_selectize("proximity_filter_spec","Choose specific samples:",{}, multiple=True),
                        ui.input_action_button("proximity_filter", "Filter", class_="btn-success"),
                        ui.input_numeric("prox_logfc_thr", "LogFC threshold", value=1.3, step=0.1,min=0),
                        ui.input_numeric("prox_pval_thr", "pvalue threshold", value=1.3, step=0.1, min=0),
                        ui.input_checkbox("prox_show_filt", "show only filtered"),
                        ui.input_select("prox_pval", "Pvalue", {"p.ord": "p.ord", "p.mod": "p.mod"}),                   
                        class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("filter") 
        ),    
        ui.nav_panel("Proximity SynGO terms",
            ui.row(
                ui.column(
                    8,
#                    ui.(""),
                    ui.output_plot("render_proximity_syngo_plot"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
#                         ui.input_select("proximity_syngo_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
#                         ui.input_action_button("proximity_syngo_filter", "Filter", class_="btn-success"),
                         class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("brain") 
        ),
        ui.nav_panel("Proximity enrichment",
            ui.row(
                ui.column(
                    8,
#                    ui.output_data_frame("render_prox_enrichment_df"),
                    ui.output_plot("render_proximity_enrich_plot", height="600px"),
                    class_="order-2 order-sm-1",
                ),
                ui.column(
                    4,
                    ui.panel_well(
#                         ui.input_select("proximity_enrichment_filter_on", "Filter on", {"norm": "Normalized", "nonnorm": "Non-normalized"}),
#                         ui.input_action_button("proximity_enrichment_filter", "Filter", class_="btn-success"),
                         class_="pb-1 mb-3",
                    ),
                ),
            ),
            icon=icon_svg("database") 
        ),     
        widths=(2,10)
     )
     ),
     title="Proximity proteomics analysis",
)

def do_imputation(data, imp_method):
   return imputation(data, imp_method)
   
def do_repl_merge(data, suffix):
   return merge_replicates(data, suffix)
   
def do_normalization(data, norm_method):
   return normalize(data)
   
def do_pca_plot(data):
   pca_res = pca_all(data)
   pca_res["group"] = pca_res.index.map(lambda x: x.split("_")[0])
   return pca_plot_all_matplotlib(pca_res, "group")
   
def do_syngo_plot(data):
   (expr_goterm_map_presynaptic, 
    proteins_goterm_map_presynaptic,
    proteins_total, 
    proteins_in_syngo, 
    proteins_presynaptic) = calc_goterms(  data, 
                                            'index', 
                                            is_presynaptic = True, 
                                            is_postsynaptic = False, 
                                            syngo_protein_cc_df = syngo_protein_cc_df, 
                                            syngo_ontology_df = syngo_ontology_df, 
                                            protein_map = protein_map)

   (expr_goterm_map_postsynaptic, 
    proteins_goterm_map_postsynaptic,
    proteins_total, 
    proteins_in_syngo, 
    proteins_postsynaptic) = calc_goterms(  data, 
                                            'index', 
                                            is_presynaptic = False, 
                                            is_postsynaptic = True, 
                                            syngo_protein_cc_df = syngo_protein_cc_df, 
                                            syngo_ontology_df = syngo_ontology_df, 
                                            protein_map = protein_map)           
       
   expr_goterm_map_sorted_presynaptic = {k: v for k, v in sorted(expr_goterm_map_presynaptic.items(), key=lambda item: -item[1])}
   expr_goterm_map_sorted_postsynaptic = {k: v for k, v in sorted(expr_goterm_map_postsynaptic.items(), key=lambda item: -item[1])}

   # taking keys to visualize (currently top 20)
   vis_keys_presynaptic = [key for key in list(expr_goterm_map_sorted_presynaptic.keys())][:20]
   vis_keys_presynaptic.reverse()

   vis_keys_postsynaptic = list(expr_goterm_map_sorted_postsynaptic.keys())[:20]
   vis_keys_postsynaptic.reverse()

   x_axis_post = [expr_goterm_map_sorted_postsynaptic[key] for key in vis_keys_postsynaptic] 
   x_axis_pre =  [expr_goterm_map_sorted_presynaptic[key] for key in vis_keys_presynaptic]

#   y_axis_post = vis_keys_postsynaptic + vis_keys_presynaptic

#   colors = ["coral"]*len(vis_keys_postsynaptic) + \
#            ["skyblue"]*len(vis_keys_presynaptic)

   fig, ax = plt.subplots()
   
   ax.barh(vis_keys_postsynaptic, x_axis_post, color = "coral", label = "postsynaptic")
   ax.barh(vis_keys_presynaptic, x_axis_pre, color = "skyblue", label = "presynaptic")
   ax.legend()
#   fig = plt.barh(y_axis, x_axis, label = x_axis, color=colors)
   
   return fig
   
def do_enrich_plot(data):
    gene_sets = ["GO_Biological_Process_2023","GO_Molecular_Function_2023","SynGO_2022","KEGG_2019_Mouse"]
    enrich_filter_df = calc_enrichment(list(data.index), 
                                    gene_sets, 
                                    protein_map = protein_map_gene,
                                    sortby='P-value', 
                                    p_threshold = 0.05, 
                                    organism="Mouse")   
    
#    print(enrich_filter_df.columns) 
    
    color_map = {"GO_Biological_Process_2023": "coral", 
                "GO_Molecular_Function_2023": "skyblue",
                "SynGO_2022": "red",
                "KEGG_2019_Mouse": "green"
                }
    
    x_axis = []
    y_axis = []
    colors = []
    
    enrich_filter_df["LogPvalue"] = enrich_filter_df["P-value"].map(lambda x: -math.log10(x))
    fig, ax = plt.subplots()  
    for gene_set in gene_sets:
        cur_df = enrich_filter_df[enrich_filter_df["Gene_set"]==gene_set].sort_values(by="LogPvalue").head(20)
        cur_df["color"] = color_map[gene_set]
        y_axis = y_axis + list(cur_df["Term"])
        x_axis = x_axis + list(cur_df["LogPvalue"])
        colors = colors + list(cur_df["color"])
        ax.barh(list(cur_df["Term"]), list(cur_df["LogPvalue"]), color = color_map[gene_set], label = gene_set)
    plt.yticks(fontsize=6)
    plt.yticks(fontsize=6)
    ax.legend()
    return fig

def server(input, output, session):

    settings = SettingsClass()

    @reactive.Calc
    def get_data():
        data = ""
        if input.upload() is None:
            return None

        @reactive.Calc
        def file_path():
            f: list[FileInfo] = input.upload()
            return f[0]["datapath"]

        @reactive.file_reader(file_path())
        def read_file():
#            global protein_name_map
            cur_path = file_path()
            settings.set_protein_file_path(cur_path)
            data = pd.read_excel(cur_path, sheet_name = settings.get_protein_active_sheet())
#            data.set_index(settings.get_protein_id_column())
#            if "protein_name" in data.columns:
#                protein_name_map = {ind: row["protein_name"] for ind, row in data.iterrows()}
#                del data["protein_name"]
            return data

        old_data_df = settings.get_val("data_df")

        if old_data_df is not None:
            return old_data_df
        data = read_file()       
        return data

 #   @reactive.event(input.go, ignore_none=False)


    @reactive.Calc
    def file_path():
        f: list[FileInfo] = input.upload()
        cur_path = f[0]["datapath"]
        settings.set_protein_file_path(cur_path)

    @reactive.Calc
    def get_imputaton_method():
        req(input.imputation_method())
        return input.imputation_method()
        
    @reactive.Calc
    def get_repl_merge_suffix():
        req(input.repl_merge_suffix())
        return input.repl_merge_suffix()
        
    @reactive.Calc
    def get_normalization_method():
        req(input.normalization_method())
        return input.normalization_method()
        
    @reactive.Calc
    def get_spec_logfc_thr():
        req(input.spec_logfc_thr())
        return input.spec_logfc_thr()
        
    @reactive.Calc
    def get_spec_pval_thr():
        req(input.spec_pval_thr())
        return input.spec_pval_thr()
        
    @reactive.Calc
    def get_spec_show_filt():
        return input.spec_show_filt()

    @reactive.Calc
    def get_spec_pval():
        req(input.spec_pval())
        return input.spec_pval()

    @reactive.Calc
    def get_specificity_filter():
        return input.specificity_filter()
        
    @reactive.Calc
    def get_prox_logfc_thr():
        req(input.prox_logfc_thr())
        return input.prox_logfc_thr()
        
    @reactive.Calc
    def get_prox_pval_thr():
        req(input.prox_pval_thr())
        return input.prox_pval_thr()
        
    @reactive.Calc
    def get_prox_show_filt():
        return input.prox_show_filt()

    @reactive.Calc
    def get_prox_pval():
        req(input.prox_pval())
        return input.prox_pval()

    @reactive.Calc
    def get_proximity_filter():
        return input.proximity_filter()
    

    @output
    @render.ui
    def data_controls():
        data = get_data()
        if data is None:
           return
        return ui.TagList(
            ui.input_select("input_sheet", "Sheet Name", settings.get_protein_sheets(), selected = settings.get_protein_active_sheet()),
            ui.input_select("input_protein_column", "Protein Uniprot ID Column", settings.get_protein_text_cols(), selected = settings.get_protein_id_column()),
            ui.input_select("input_protein_name", "Protein Name Column", [""]+settings.get_protein_text_cols(), selected=settings.get_protein_name_column()),
            ui.input_selectize("input_data_cols","Data columns",
                 choices = settings.get_protein_num_cols(), 
                 selected = settings.get_protein_data_cols(),
                 multiple=True),
        ) 
    
    @reactive.effect
    @reactive.event(input.input_sheet)
    def input_sheet_change():
        settings.set_protein_active_sheet(input.input_sheet.get())

    @reactive.effect
    @reactive.event(input.input_protein_column)
    def input_input_protein_column_change():
        settings.set_protein_id_column(input.input_protein_column.get())

    @reactive.effect
    @reactive.event(input.input_protein_name)
    def input_protein_name_change():
        settings.set_protein_name_column(input.input_protein_name.get())

    @reactive.effect
    @reactive.event(input.input_data_cols)
    def input_protein_data_cols_change():
        settings.set_protein_data_cols([col for col in input.input_data_cols.get()])

    @output
    @render.data_frame
    def render_original_df():
        data = settings.get_protein_display_data()
        if data is None:
           return
        render_data = data.copy()
        return render.DataGrid(render_data, row_selection_mode="single") 

    @output
    @render.data_frame
    def render_samples_conditions_df():
        data = get_data()
        if data is None:
           return
        samp_cond_df = pd.DataFrame({"samples": data.columns, "conditions": data.columns})
        samp_cond_df["conditions"] = samp_cond_df["conditions"].map(lambda x: x.split("_")[0])
        return render.DataGrid(samp_cond_df, row_selection_mode="single", editable=True)


    @output
    @render.data_frame
    def render_imputed_df():
        data = get_data()
        if data is None:
           return
        imp_method = get_imputaton_method()
#        print("*****************Stopping**************")
#        app.stop()
        data = do_imputation(data, imp_method)
        
        render_data = data.copy()
        render_data.insert(0, "Protein", data.index)
        
        return render.DataGrid(render_data, row_selection_mode="single") 
        
    @output
    @render.data_frame
    def render_merged_df():
        data = get_data()
        if data is None:
           return
           
        imp_method = get_imputaton_method()
        data = do_imputation(data, imp_method)
        
        repl_nerge_suffix = get_repl_merge_suffix()
        data = do_repl_merge(data, repl_nerge_suffix)
        
        ui.update_selectize(
            "specificity_filter_base",
            choices={c:c for c in data.columns}
        )
        ui.update_selectize(
            "specificity_filter_spec",
            choices={c:c for c in data.columns}
        )
        
        ui.update_selectize(
            "proximity_filter_base",
            choices={c:c for c in data.columns}
        )
        ui.update_selectize(
            "proximity_filter_spec",
            choices={c:c for c in data.columns}
        )



        render_data = data.copy()        
        render_data.insert(0, "Protein", data.index)

        return render.DataGrid(render_data, row_selection_mode="single") 

    @output
    @render.data_frame
    def render_norm_df():
        global norm_data
        data = get_data()
        if data is None:
           return

        imp_method = get_imputaton_method()
        data = do_imputation(data, imp_method)

        repl_nerge_suffix = get_repl_merge_suffix()
        data = do_repl_merge(data, repl_nerge_suffix)
        
        norm_method = get_normalization_method()
        data = do_normalization(data, norm_method)
        
        norm_data = data.copy()
        data = data.round(decimals=2)
        data.insert(0, "Protein", data.index)
        if protein_name_map is not None:
            data.insert(1, "Protein name", data.index)
            data["Protein name"] = data["Protein name"].map(protein_name_map)
        return render.DataGrid(data, row_selection_mode="single")
        
    @output
    @render.plot
    def imputation_pca_plot():
        data = get_data()
        if data is None:
           return

        imp_method = get_imputaton_method()
        data = do_imputation(data, imp_method)

        fig = do_pca_plot(data)
        return fig
        
    @output
    @render.plot
    def repl_merge_pca_plot():
        data = get_data()
        if data is None:
           return

        imp_method = get_imputaton_method()
        data = do_imputation(data, imp_method)
        
        repl_nerge_suffix = get_repl_merge_suffix()
        data = do_repl_merge(data, repl_nerge_suffix)

        fig = do_pca_plot(data)
        return fig

    @output
    @render.plot
    def normalization_pca_plot():
        data = get_data()
        if data is None:
           return

        imp_method = get_imputaton_method()
        data = do_imputation(data, imp_method)

        repl_nerge_suffix = get_repl_merge_suffix()
        data = do_repl_merge(data, repl_nerge_suffix)

        norm_method = get_normalization_method()
        data = do_normalization(data, norm_method)

        fig = do_pca_plot(data)
        return fig
        
    def calc_specificity_filt_df(specif_df, logfc_thr, pval_thr, pval_field, logfc_field = "logFC"):
#         print(specif_df.columns)
         res_df = specif_df[[pval_field, logfc_field]] 
         res_df["logp"] = res_df[pval_field].map(lambda x: -math.log10(x))
         res_df = res_df[res_df[logfc_field].ge(logfc_thr) & res_df["logp"].ge(pval_thr)]        
         return res_df
    
    @ui.bind_task_button(button_id="shutdown")
    @reactive.extended_task
    async def sum_values():
        print("*****************SSSSSSSSSSSSSSSSSSSSSSSS")
#        os.kill(p.pid, signal.CTRL_C_EVENT)
#        await app.stop()
        return
        
    @reactive.effect
    @reactive.event(input.shutdown)
    def btn_click():
        tasks = asyncio.all_tasks()
        print(len(tasks))
        try:
           for task in tasks:
         # request the task cancel
               task.cancel()
#               break
#               exc = task.exception()
        except:
           print("Stopped")
#         os.kill(p.pid, signal.CTRL_C_EVENT)
#        sys.exit()
#        sum_values()

    
    @output
    @render.data_frame
#    @reactive.event(input.specificity_filter, ignore_none=False)   
    def render_specificity_df():
        global specificity_res_df
        global specificity_filt_df
        global specificity_data_filt_df
        global num_spec_clicks


#        print("*****************Stopping**************")
#        app.stop()

        print("***************soec0")        
        if norm_data is None:
            return        
            
        logfc_thr = get_spec_logfc_thr()
        p_thr = get_spec_pval_thr()
        pval_col = get_spec_pval()
        show_filt = get_spec_show_filt()     
        btn_pressed = get_specificity_filter()
        
        group1 = list(input.specificity_filter_base())
        group2 = list(input.specificity_filter_spec())
        if num_spec_clicks!=btn_pressed:
             num_spec_clicks = btn_pressed
             specificity_res_df = diff_abundance(norm_data, group1, group2, log_transform = True)

             
        if specificity_res_df is None:
             return
        
        specificity_filt_df = calc_specificity_filt_df(specificity_res_df, 
                                                       logfc_thr = input.spec_logfc_thr(), 
                                                       pval_thr = input.spec_pval_thr(), 
                                                       pval_field = input.spec_pval())
                                                       
        if show_filt:
            return specificity_filt_df
        return specificity_res_df
        
    @output
    @render.plot
    def render_specificity_volcano():
        global specificity_res_df
        
        if specificity_res_df is None:
            return
            
        logfc_thr = get_spec_logfc_thr()
        p_thr = get_spec_pval_thr()
        
        pval_col = get_spec_pval()

        fig = volcano_plot_matplotlib(specificity_res_df, pval_col, "logFC", logFC_thr = logfc_thr, p_thr=p_thr)
        
        specificity_filt_df = calc_specificity_filt_df(specificity_res_df, 
                                                       logfc_thr = input.spec_logfc_thr(), 
                                                       pval_thr = input.spec_pval_thr(), 
                                                       pval_field = input.spec_pval())        
        return fig

    @output
    @render.plot
    def render_specificity_syngo_plot():
        global specificity_filt_df

        if specificity_filt_df is None:
            return

        fig = do_syngo_plot(specificity_filt_df)   

        return fig


    @output
    @render.plot
    def render_specificity_enrich_plot():
        global specificity_filt_df

        if specificity_filt_df is None:
            return

        fig = do_enrich_plot(specificity_filt_df) 
        
        return fig

    @output
    @render.plot
    def render_proximity_syngo_plot():
        global proximity_filt_df

        if proximity_filt_df is None:
            return

        fig = do_syngo_plot(proximity_filt_df)   

        return fig


    @output
    @render.plot
    def render_proximity_enrich_plot():
        global proximity_filt_df

        if proximity_filt_df is None:
            return

        fig = do_enrich_plot(proximity_filt_df) 
        
        return fig

    @output
    @render.data_frame
#    @reactive.event(input.specificity_filter, ignore_none=False)   
    def render_proximity_df():
        global proximity_res_df
        global proximity_filt_df
        global specificity_filt_df
        global num_prox_clicks
        
        print("************************0")
        if norm_data is None:
            return
        
        if specificity_filt_df is None:
            print("Mot")
            return        
        
        print("************************1")

        logfc_thr = get_prox_logfc_thr()
        p_thr = get_prox_pval_thr()
        pval_col = get_prox_pval()
        show_filt = get_prox_show_filt()     
        btn_pressed = get_proximity_filter()
#        print("Stopping")
#        app.stop()
        
        group1 = list(input.proximity_filter_base())
        group2 = list(input.proximity_filter_spec())
        if num_prox_clicks!=btn_pressed:
             num_prox_clicks = btn_pressed
             data_filt_df = norm_data.loc[specificity_filt_df.index]
             proximity_res_df = diff_abundance(data_filt_df, group1, group2, log_transform = True)

             
        if proximity_res_df is None:
             return
        
        proximity_filt_df = calc_specificity_filt_df(proximity_res_df, 
                                                       logfc_thr = input.prox_logfc_thr(), 
                                                       pval_thr = input.prox_pval_thr(), 
                                                       pval_field = input.prox_pval())
        if show_filt:
            return proximity_filt_df
        return proximity_res_df
        
    @output
    @render.plot
    def render_proximity_volcano():
        global proximity_filt_df
        
        if proximity_res_df is None:
            return
            
        logfc_thr = get_prox_logfc_thr()
        p_thr = get_prox_pval_thr()
        
        pval_col = get_prox_pval()

        fig = volcano_plot_matplotlib(proximity_res_df, pval_col, "logFC", logFC_thr = logfc_thr, p_thr=p_thr)
        
        proximity_filt_df = calc_specificity_filt_df(proximity_res_df, 
                                                       logfc_thr = input.prox_logfc_thr(), 
                                                       pval_thr = input.prox_pval_thr(), 
                                                       pval_field = input.prox_pval())        
        return fig




    
num_spec_clicks = 0
num_prox_clicks = 0
norm_data = None
specificity_res_df = None
specificity_filt_df = None
proximity_res_df = None
proximity_filt_df = None
print("Starting server")
try:
   app = App(app_ui, server, debug=True)
   exc = app.exception()
except:
   print("Cancelled")
