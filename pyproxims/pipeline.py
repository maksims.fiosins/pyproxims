#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
import os
import json
import math
import re

from diff_abundance import diff_abundance, volcano_plot
from imputation import imputation
from ontology_annotation import calc_goterms, add_in_syngo, calc_enrichment
from pca import pca, pca_plot
from normalization import normalize, merge_replicates
from pnas import add_pnas
from lysoendo import add_lysoendo

import plotly.express as px

from collections import OrderedDict

def write_output_matrix(df, filename, index=True):
   df.to_csv(os.path.join(args.output_folder,filename), sep = "\t", index = index)
   
def write_output_plot(plot, filename):
   plot.write_html(os.path.join(args.output_folder,filename)) 
   
def str_chinks(lst, ln, sep):
    return [sep.join(lst[x:(x+ln)]) for x in range(0, len(lst), ln)]
    
def add_gene_column(df):
    df.insert(0, "protein_name", df.index.map(protein_map))
    return df

parser = argparse.ArgumentParser(
                    prog = 'proteomics pipeline',
                    description = 'This is nextflow-based proteomics pipeline')
# Pathes    
parser.add_argument('--input_file', help='input folder')
parser.add_argument('--metadata_file', help='metadata file')
parser.add_argument('--output_folder', help='output folder')
# General
parser.add_argument('--imputation_method', help='imputation method to be used (none, zero, ...)')
parser.add_argument('--log_transform', help='log transform for diff. abundance', default='false', nargs='?')
parser.add_argument('--pca', help='perform pca?', nargs='?', default='false')
parser.add_argument('--replicate_merge', help='merge replicates?', nargs='?', default='true')
parser.add_argument('--replicate_suffix', default="T\d+")
# Filtering
parser.add_argument('--filter_na', help='filter by na values?', nargs='?', default='false')
parser.add_argument('--filter_na_field', help='metadata field for na filtering (if specified, taken from metadata file; if not specified, filter_na_group is expected to be a list of field names)', nargs='?', default='false')
parser.add_argument('--filter_na_group', help='group for na filtering')
parser.add_argument('--filter_na_fraction', help='na fraction theshold for filtering')

parser.add_argument('--filter_norm', help='normlize data for filtering?', nargs='?', default='false')
parser.add_argument('--filter_pmod', help='filter on pmod for protein annotation?', nargs='?', default='false')
parser.add_argument('--filter_pord', help='filter on pord for protein annotation?', nargs='?', default='false')
parser.add_argument('--filter_logfc', help='filter on logfc for protein annotation?', nargs='?', default='false')
parser.add_argument('--filter_metadata_field', help='metadata field for filtering (if specified, taken from metadata file; if not specified, group1 and group2 are expected to be lists of field names)', nargs='?', default="")
parser.add_argument('--filter_group1', help='group1 for differential abundance in filtering')
parser.add_argument('--filter_group2', help='group2 for differential abundance in filtering')
parser.add_argument('--filter_p_thr', help='p theshold for filtering')
parser.add_argument('--filter_logfc_thr', help='logfc theshold for filtering')
# Diff. abundance
parser.add_argument('--diff_abundance', help='perform differential abundance?', nargs='?', default='false')
parser.add_argument('--da_norm', help='normlize data for filtering?', nargs='?', default='false')
parser.add_argument('--da_filter_pmod', help='filter on pmod for protein annotation?', nargs='?', default='false')
parser.add_argument('--da_filter_pord', help='filter on pord for protein annotation?', nargs='?', default='false')
parser.add_argument('--da_filter_logfc', help='filter on logfc for protein annotation?', nargs='?', default='false')
parser.add_argument('--da_metadata_field', help='metadata field for filtering (if specified, taken from metadata file; if not specified, group1 and group2 are expected to be lists of field names)', nargs='?', default="")
parser.add_argument('--da_group1', help='group1 for differential abundance in filtering')
parser.add_argument('--da_group2', help='group2 for differential abundance in filtering')
parser.add_argument('--da_p_thr', help='p theshold for filtering')
parser.add_argument('--da_logfc_thr', help='logfc theshold for filtering')
# PNAS annotation
parser.add_argument('--pnas_annotate', help='annotate by PNAS?', nargs='?', default='false')
parser.add_argument('--pnas_data', help='data file with PNAS data', nargs='?')
# Lysosomal/endosomal annotation
parser.add_argument('--lyso_annotate', help='annotate by lysosomal DB?', nargs='?', default='false')
parser.add_argument('--lyso_data', help='file with lysosomal data', nargs='?')
parser.add_argument('--lyso_sheet', help='sheet with lysosomal data', nargs='?')
parser.add_argument('--endo_annotate', help='annotate by endosomal DB?', nargs='?', default='false')
parser.add_argument('--endo_data', help='file with endosomal data', nargs='?')
parser.add_argument('--endo_sheet', help='sheet with endosomal data', nargs='?')
parser.add_argument('--endo_manual_sheet', help='sheet with manual endosomal data', nargs='?')
# Syngo annotation
parser.add_argument('--syngo_annotate', help='whether to perform syngo annotation', nargs='?', default='false')
parser.add_argument('--syngo_protein_cc', help='protein annotation file', nargs='?')
parser.add_argument('--syngo_ontology', help='syngo ontology file', nargs='?')
# Ontology annotation
parser.add_argument('--ontology_annotate', help='whether to perform enrichr annotation', nargs='?', default='false')
parser.add_argument('--ontology_sets', help='enrichr sets for annotation')
parser.add_argument('--ontology_p_threshold', help='p threshold for enriched pathways')
# Protein name mapping
parser.add_argument('--protein_map_file', help='csv file for protein name mapping', nargs='?')
parser.add_argument('--protein_map_source_field', help='field to use for mapping', nargs='?')
parser.add_argument('--protein_map_dest_field', help='field to map to', nargs='?')
parser.add_argument('--protein_map_gene_field', help='field to map to gene', nargs='?')

args = parser.parse_args()

args.replicate_suffix = "_T\d+"

if args.filter_p_thr:
   filter_p_thr = float(args.filter_p_thr)

if args.filter_logfc_thr:
   filter_logfc_thr = float(args.filter_logfc_thr)
   
if args.da_p_thr:
   da_p_thr = float(args.da_p_thr)

if args.da_logfc_thr:
   da_logfc_thr = float(args.da_logfc_thr)

if not args.input_file:
   raise ValueError('Input file is not specified')

if not args.output_folder:
   raise ValueError('Output folder is not specified')
   
if not os.path.isdir(args.output_folder):
   os.mkdir(args.output_folder)

# Reading data
data_df_orig = pd.read_csv(args.input_file, sep = "\t")
# write_output_matrix(data_df, "original_matrix.csv")

metadata_json = None
if args.metadata_file:
    with open(args.metadata_file, 'r') as f:
         metadata_json = json.load(f)

# Set first column as index, if it is not numeric and leave only numerical data
first_column = data_df_orig.columns[0]
if data_df_orig[first_column].dtype == object:
   data_df_orig = data_df_orig[~data_df_orig[first_column].isna()]
   data_df_orig.index = data_df_orig[first_column]
   data_df_orig.drop(columns = [first_column], inplace = True)
data_df_orig = data_df_orig.select_dtypes(include=np.number)

# Read mapping file
protein_map = {}
protein_map_gene = {}
if args.protein_map_file:
    protein_map_df = pd.read_csv(args.protein_map_file, sep = "\t")
    if args.protein_map_source_field not in protein_map_df.columns:
        raise ValueError(f"column {args.protein_map_source_field} not found in map file; available columns: {protein_map_df.columns}")
    if args.protein_map_dest_field not in protein_map_df.columns:
        raise ValueError(f"column {args.protein_map_dest_field} not found in map file; available columns: {protein_map_df.columns}")
    protein_map = {row[args.protein_map_source_field]: row[args.protein_map_dest_field] for index, row in protein_map_df.iterrows()}
    protein_map_gene = {row[args.protein_map_source_field]: row[args.protein_map_gene_field] for index, row in protein_map_df.iterrows()}

# Replicate merge
if args.replicate_merge != "false":
    data_df_repl_merged = merge_replicates(data_df_orig, args.replicate_suffix)
else:
    data_df_repl_merged = data_df_orig.copy()

# Normalization
if args.filter_norm != "false" or args.da_norm != "false":
    data_df_norm = normalize(data_df_repl_merged)
else:
    data_df_norm = None

# Imputation
if args.imputation_method != 'none':
    data_df_imputed = imputation(data_df_repl_merged, method = args.imputation_method, log_transform = (args.log_transform.lower() != 'false'))
    if data_df_norm is not None:
         data_df_imputed_norm = imputation(data_df_norm, method = args.imputation_method, log_transform = (args.log_transform.lower() != 'false'))
    else:
         data_df_imputed_norm = None
#    write_output_matrix(data_df, "imputed_matrix.csv")
else:
    data_df_imputed = data_df_repl_merged.copy()
    if data_df_norm is not None:
         data_df_imputed_norm = data_df_norm.copy()
    else:
         data_df_imputed_norm = None  

filter_group1 = args.filter_group1.split(",")
filter_group2 = args.filter_group2.split(",")
filt_samples_not_merged = set(filter_group1) | set(filter_group2)

if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
    if args.filter_metadata_field:
       if metadata_json is None:
           raise ValueError("filter metadata_field parameter is specified, then metadata_file should be specified")
       filter_group1 = [list(meta_elem.values())[0]["sample"]["name"] for meta_elem in metadata_json if "sample" in list(meta_elem.values())[0] and list(meta_elem.values())[0]["sample"][args.filter_metadata_field] in filter_group1]
       filter_group2 = [list(meta_elem.values())[0]["sample"]["name"] for meta_elem in metadata_json if "sample" in list(meta_elem.values())[0] and list(meta_elem.values())[0]["sample"][args.filter_metadata_field] in filter_group2]
       filt_samples_not_merged = set(filter_group1) | set(filter_group2)
       if len(filter_group1) == 0:
           raise ValueError("filter group1 is empty")
       if len(filter_group2) == 0:
           raise ValueError("filter group2 is empty")
       if args.replicate_merge != "false":
           filter_group1 = list(set([re.sub(args.replicate_suffix+"$", "", field) for field in filter_group1]))
           filter_group2 = list(set([re.sub(args.replicate_suffix+"$", "", field) for field in filter_group2]))

    filter_group_df = pd.DataFrame({"group1":[",".join(filter_group1)], "group2":[",".join(filter_group2)]})
#    write_output_matrix(group_df, "differential_abundance_groups.csv", index = False)

    incorrect_columns = []
    for elem in filter_group1 + filter_group2:
        if elem not in data_df_imputed.columns:
            incorrect_columns.append(elem)
            
    if incorrect_columns:
        raise ValueError(f"the columns {incorrect_columns} are invalid (valid columns are {data_df_imputed.columns})")

    if args.filter_norm != 'false':
        data_df_for_filt = data_df_imputed_norm
    else:
        data_df_for_filt = data_df_imputed   

    filt_da_res = diff_abundance(data_df_for_filt, group1 = filter_group1, group2 = filter_group2, log_transform = (args.log_transform.lower()!='false'))
    filt_da_res_orig = filt_da_res.copy()
#    write_output_matrix(da_res, "differential_abundance.csv")

    filt_da_plot1 = volcano_plot(filt_da_res, p_axis = 'p.ord', logfc_axis = 'logFC', p_thr = filter_p_thr if args.filter_pmod != "false" or args.filter_pord != "false" else None, logFC_thr = filter_logfc_thr if args.filter_logfc != "false" else None, protein_map = protein_map)
    filt_da_plot2 = volcano_plot(filt_da_res, p_axis = 'p.mod', logfc_axis = 'logFC', p_thr = filter_p_thr if args.filter_pmod != "false" or args.filter_pord != "false" else None, logFC_thr = filter_logfc_thr if args.filter_logfc != "false" else None, protein_map = protein_map)
    write_output_plot(filt_da_plot1, "volcano_plot_filt_p_ord.html")
    write_output_plot(filt_da_plot2, "volcano_plot_filt_p_mod.html")

    filt_da_res_filt = filt_da_res.copy()
    if args.filter_pmod and args.filter_pmod != "false":
        filt_da_res_filt["log_pmod"] = filt_da_res_filt["p.mod"].apply(lambda x: -math.log10(x))
        filt_da_res_filt = filt_da_res_filt[filt_da_res_filt["log_pmod"].ge(filter_p_thr)]
        
    if args.filter_pord and args.filter_pord != "false":
        filt_da_res_filt["log_pord"] = filt_da_res_filt["p.ord"].apply(lambda x: -math.log10(x))
        filt_da_res_filt = filt_da_res_filt[da_res_filt["log_pord"].ge(filter_p_thr)]
        
    if args.filter_logfc and args.filter_logfc != "false":
        filt_da_res_filt = filt_da_res_filt[filt_da_res_filt["logFC"].ge(filter_logfc_thr)]
        
    filter_index = filt_da_res_filt.index
    data_df_filt = data_df_imputed.loc[filter_index]
    data_df_norm_filt = data_df_imputed_norm.loc[filter_index]
#    write_output_matrix(data_df_filt, "filtered_matrix.csv")
    
    if args.pca and args.pca != "false":
        pca_df_filt = pca(data_df_filt, filter_group1, filter_group2, legend_name = args.filter_metadata_field, group1_name = args.filter_group1, group2_name = args.filter_group2, log_transform = (args.log_transform.lower()!='false'))
        pca_fig_filt = pca_plot(pca_df_filt, filter_group1, filter_group2, legend_name = args.filter_metadata_field, group1_name = args.filter_group1, group2_name = args.filter_group2)
        write_output_plot(pca_fig_filt, "pca_filt.html")
else:
    data_df_filt = data_df_imputed.copy()
    if data_df_imputed_norm is not None:
        data_df_norm_filt = data_df_imputed_norm.copy()
    else:
        data_df_norm_filt = None

if args.filter_na != "false":
    filter_na_group = args.filter_na_group.split(",")
    if args.filter_na_field:
       if metadata_json is None:
           raise ValueError("if filter_na field parameter is specified, then metadata_file should be specified")
       filter_na_group = [list(meta_elem.values())[0]["sample"]["name"] for meta_elem in metadata_json if "sample" in list(meta_elem.values())[0] and list(meta_elem.values())[0]["sample"][args.filter_na_field] in filter_na_group]
   
    if filter_na_group == []:
        raise ValueError(f'You specify to filter NA, but the field set is empty')
    num_nas = data_df_orig[filter_na_group].isnull().sum(axis=1)
    na_thr = len(filter_na_group)*float(args.filter_na_fraction)
    na_ind = num_nas.loc[lambda x: x<na_thr].index
    
    data_df_filt_na = data_df_filt.loc[data_df_filt.index.intersection(na_ind)]
    data_df_norm_filt_na = data_df_norm_filt.loc[data_df_norm_filt.index.intersection(na_ind)]
else:
    data_df_filt_na = data_df_filt.copy()
    data_df_norm_filt_na = data_df_norm_filt.copy()

da_group1 = args.da_group1.split(",")
da_group2 = args.da_group2.split(",")
da_samples_not_merged = set(da_group1) | set(da_group2)

if args.diff_abundance != "false":
    if args.da_metadata_field:
       if metadata_json is None:
           raise ValueError("da metadata_field parameter is specified, then metadata_file should be specified")
       da_group1 = [list(meta_elem.values())[0]["sample"]["name"] for meta_elem in metadata_json if "sample" in list(meta_elem.values())[0] and list(meta_elem.values())[0]["sample"][args.da_metadata_field] in da_group1]
       da_group2 = [list(meta_elem.values())[0]["sample"]["name"] for meta_elem in metadata_json if "sample" in list(meta_elem.values())[0] and list(meta_elem.values())[0]["sample"][args.da_metadata_field] in da_group2]
       da_samples_not_merged = set(da_group1) | set(da_group2)
       if len(da_group1) == 0:
           raise ValueError("filter group1 is empty")
       if len(da_group2) == 0:
           raise ValueError("filter group2 is empty")
       if args.replicate_merge != "false":
           da_group1 = list(set([re.sub(args.replicate_suffix+"$", "", field) for field in da_group1]))
           da_group2 = list(set([re.sub(args.replicate_suffix+"$", "", field) for field in da_group2]))

    da_group_df = pd.DataFrame({"group1":[",".join(da_group1)], "group2":[",".join(da_group2)]})
#    write_output_matrix(group_df, "differential_abundance_groups.csv", index = False)

    incorrect_columns = []
    for elem in da_group1 + da_group2:
        if elem not in data_df_filt_na.columns:
            incorrect_columns.append(elem)
            
    if incorrect_columns:
        raise ValueError(f"the columns {incorrect_columns} are invalid (valid columns are {data_df_filt.columns})")

    if args.da_norm != 'false':
        data_df_for_da = data_df_norm_filt_na
    else:
        data_df_for_da = data_df_filt_na

    da_res = diff_abundance(data_df_for_da, group1 = da_group1, group2 = da_group2, log_transform = (args.log_transform.lower()!='false'))
    da_res_orig = da_res.copy()
#    write_output_matrix(da_res, "differential_abundance.csv")

    da_plot1 = volcano_plot(da_res, p_axis = 'p.ord', logfc_axis = 'logFC', p_thr = da_p_thr if args.da_filter_pmod != "false" or args.da_filter_pord != "false" else None, logFC_thr = da_logfc_thr if args.da_filter_logfc != "false" else None, protein_map = protein_map)
    da_plot2 = volcano_plot(da_res, p_axis = 'p.mod', logfc_axis = 'logFC', p_thr = da_p_thr if args.da_filter_pmod != "false" or args.da_filter_pord != "false" else None, logFC_thr = da_logfc_thr if args.da_filter_logfc != "false" else None, protein_map = protein_map)
    write_output_plot(da_plot1, "volcano_plot_da_p_ord.html")
    write_output_plot(da_plot2, "volcano_plot_da_p_mod.html")

    if args.da_filter_pmod and args.da_filter_pmod != "false":
        da_res["log_pmod"] = da_res["p.mod"].apply(lambda x: -math.log10(x))
        da_res = da_res[da_res["log_pmod"].ge(da_p_thr)]
        
    if args.da_filter_pord and args.da_filter_pord != "false":
        da_res["log_pord"] = da_res["p.ord"].apply(lambda x: -math.log10(x))
        da_res = da_res[da_res["log_pord"].ge(da_p_thr)]
        
    if args.da_filter_logfc and args.da_filter_logfc != "false":
        da_res = da_res[da_res["logFC"].ge(da_logfc_thr)]
        
    da_index = da_res.index
    da_data_df_filt = data_df_for_da.loc[da_index]
#    write_output_matrix(data_df_filt, "filtered_matrix.csv")
    
    if args.pca and args.pca != "false":
        pca_df_da = pca(da_data_df_filt, da_group1, da_group2, legend_name = args.da_metadata_field, group1_name = args.da_group1, group2_name = args.da_group2, log_transform = (args.log_transform.lower()!='false'))
        pca_fig_da = pca_plot(pca_df_da, da_group1, da_group2, legend_name = args.da_metadata_field, group1_name = args.da_group1, group2_name = args.da_group2)
        write_output_plot(pca_fig_filt, "pca_filt.html")
else:
    da_data_df_filt = data_df_filt.copy()

if args.pca != "false":
    if args.diff_abundance != "false":
        pca_df = pca(data_df_imputed, da_group1, da_group2, legend_name = args.da_metadata_field, group1_name = args.da_group1, group2_name = args.da_group2, log_transform = (args.log_transform.lower()!='false'))
        pca_fig = pca_plot(pca_df, da_group1, da_group2, legend_name = args.da_metadata_field, group1_name = args.da_group1, group2_name = args.da_group2)
    else:
        pca_df = pca(data_df_imputed, None, None, log_transform = (args.log_transform.lower()!='false'))
        pca_fig = pca_plot(pca_df, None, None)
    write_output_plot(pca_fig, "pca_orig.html")

if args.syngo_annotate and args.syngo_annotate != "false":
    if not args.syngo_protein_cc:
        raise ValueError('You specified syngo_annotate, but did not specify syngo_protein_cc')
    syngo_protein_cc_df = pd.read_csv(args.syngo_protein_cc, sep = "\t")
    if not args.syngo_ontology:
        raise ValueError('You specified syngo_annotate, but did not specify syngo_ontology')
    syngo_ontology_df = pd.read_csv(args.syngo_ontology, sep = "\t")
    (expr_goterm_map_presynaptic, 
     proteins_goterm_map_presynaptic,
     proteins_total, 
     proteins_in_syngo, 
     proteins_presynaptic) = calc_goterms(   da_data_df_filt, 
                                             'index', 
                                             is_presynaptic = True, 
                                             is_postsynaptic = False, 
                                             syngo_protein_cc_df = syngo_protein_cc_df, 
                                             syngo_ontology_df = syngo_ontology_df, 
                                             protein_map = protein_map)

    (expr_goterm_map_postsynaptic, 
     proteins_goterm_map_postsynaptic,
     proteins_total, 
     proteins_in_syngo, 
     proteins_postsynaptic) = calc_goterms( da_data_df_filt, 
                                             'index', 
                                             is_presynaptic = False, 
                                             is_postsynaptic = True, 
                                             syngo_protein_cc_df = syngo_protein_cc_df, 
                                             syngo_ontology_df = syngo_ontology_df, 
                                             protein_map = protein_map)

    expr_goterm_map_sorted_presynaptic = {k: v for k, v in sorted(expr_goterm_map_presynaptic.items(), key=lambda item: -item[1])}
    expr_goterm_map_sorted_postsynaptic = {k: v for k, v in sorted(expr_goterm_map_postsynaptic.items(), key=lambda item: -item[1])}

    # taking keys to visualize (currently top 20)
    vis_keys_presynaptic = [key for key in list(expr_goterm_map_sorted_presynaptic.keys())][:20]
    vis_keys_presynaptic.reverse()

    vis_keys_postsynaptic = list(expr_goterm_map_sorted_postsynaptic.keys())[:20]
    vis_keys_postsynaptic.reverse()
    df = px.data.tips()

    x_axis = [expr_goterm_map_sorted_postsynaptic[key] for key in vis_keys_postsynaptic] +\
             [expr_goterm_map_sorted_presynaptic[key] for key in vis_keys_presynaptic]

    y_axis = vis_keys_postsynaptic + vis_keys_presynaptic

    custom_data = [["("+str(count) + "): " + "<BR>".join(elem) for elem, count in [(str_chinks(sorted(list(proteins_goterm_map_postsynaptic[key])),5,","), len(proteins_goterm_map_postsynaptic[key])) for key in vis_keys_postsynaptic]] + \
                   ["("+str(count) + "): " + "<BR>".join(elem) for elem, count in [(str_chinks(sorted(list(proteins_goterm_map_presynaptic[key])),5,","), len(proteins_goterm_map_presynaptic[key])) for key in vis_keys_presynaptic]]]

    colors = [px.colors.qualitative.Plotly[1],]*len(vis_keys_postsynaptic) + \
             [px.colors.qualitative.Plotly[0],]*len(vis_keys_presynaptic)

    fig = px.bar(x=x_axis, y=y_axis, 
                 custom_data = custom_data,
                 orientation='h',
                 height=600,
                 title="Proteins total: %s, in syngo: %s, presynaptic: %s, postsynaptic: %s"%(proteins_total, proteins_in_syngo, proteins_presynaptic, proteins_postsynaptic))

    fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey')
    fig.update_yaxes(dtick = 1)
    fig.update_traces(hovertemplate='<br>Proteins:%{customdata[0]}<br>')
    fig.update_traces(marker_color=colors)

    fig.update_layout(
    #    title="Differential abundance",
        xaxis_title="#proteins",
        yaxis_title="syngo terms",
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'
    )

    fig.update_layout(
    title={
        'x': 0.5,
        'y': 0.95,
        'xanchor': 'center',
        'yanchor': 'top',
        'font':{
           'size': 30
        }})

    write_output_plot(fig, "ontology_annotation_plot.html")
    annot_df = pd.DataFrame({"ontology term": y_axis, "number of proteins": x_axis})
    write_output_matrix(annot_df, "ontology_annotation_plot_data.csv", index = False)
    
#    fig.write_html("presynaptic_enrichment.html")
#    print("Proteins total: %s, in syngo: %s, presynaptic: %s, postsynaptic: %s"%(proteins_total, proteins_in_syngo, proteins_presynaptic, proteins_postsynaptic))  
    
#    calc_goterms(data_df, "index", True, False, syngo_protein_cc_df, syngo_ontology_df, uniprot_dict = uniprot_dict_df)

if args.ontology_annotate and args.ontology_annotate != "false":
    gene_sets = args.ontology_sets.split(",")
    enrich_filter_df = calc_enrichment(list(data_df_filt_na.index), 
                                    gene_sets, 
                                    protein_map = protein_map_gene,
                                    sortby='P-value', 
                                    p_threshold = float(args.ontology_p_threshold), 
                                    organism="Mouse")

    enrich_da_df = calc_enrichment(list(da_data_df_filt.index), 
                                    gene_sets, 
                                    protein_map = protein_map_gene,
                                    sortby='P-value', 
                                    p_threshold = float(args.ontology_p_threshold), 
                                    organism="Mouse")

output_dict = OrderedDict()

all_samples = set()
if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
   all_samples.update(set(filter_group1))
   all_samples.update(set(filter_group2))

if args.diff_abundance and args.diff_abundance != "false":
   all_samples.update(set(da_group1))
   all_samples.update(set(da_group2))

# Preserve original order
all_samples = [samp for samp in data_df_repl_merged.columns if samp in all_samples]
all_samples_orig = [samp for samp in data_df_orig if samp in filt_samples_not_merged or samp in da_samples_not_merged]

da_result_fields = ["logFC", "p.ord", "p.mod", "logp.ord", "logp.mod"]

output_dict["original_matrix"] = add_gene_column(data_df_orig[all_samples_orig])
output_dict["merged_replicates"] = add_gene_column(data_df_repl_merged[all_samples])
data_df_imputed_with_filt_da = data_df_imputed[all_samples]
if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
     data_df_imputed_with_filt_da = pd.merge(data_df_imputed[all_samples], filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True)
output_dict["imputed_matrix"] = add_gene_column(data_df_imputed_with_filt_da)
if data_df_norm is not None:
     data_df_norm_with_filt_da = data_df_norm[all_samples]
     if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
          data_df_norm_with_filt_da = pd.merge(data_df_norm[all_samples], filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True)
     output_dict["normalized_matrix"] = add_gene_column(data_df_norm_with_filt_da)
if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
     output_dict["filter_diff_abundance_groups"] = filter_group_df
     output_dict["filter_diff_abundance_results"] = add_gene_column(filt_da_res_orig[da_result_fields].add_suffix("_spec"))
     data_df_filt_with_filt_da = pd.merge(data_df_filt[all_samples], filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True, how='left')
     data_df_norm_filt_with_filt_da = pd.merge(data_df_norm_filt[all_samples], filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True, how='left')
     data_df_filt_with_filt_da_da = data_df_filt_with_filt_da
     data_df_norm_filt_with_filt_da_da = data_df_norm_filt_with_filt_da
     if args.diff_abundance and args.diff_abundance != "false":
          data_df_filt_with_filt_da_da = pd.merge(data_df_filt_with_filt_da, da_res_orig[da_result_fields].add_suffix("_prox"), left_index=True, right_index=True, how='left')
          data_df_norm_filt_with_filt_da_da = pd.merge(data_df_norm_filt_with_filt_da, da_res_orig[da_result_fields].add_suffix("_prox"), left_index=True, right_index=True, how='left')
     output_dict["filtered_matrix"] = \
            add_pnas(
                 data = add_in_syngo(
                           data = add_lysoendo(
                                     data = add_gene_column(
                                              data_df_filt_with_filt_da_da), 
                                     args = args
                                  ),
                           syngo_protein_cc_df = syngo_protein_cc_df,
                           args = args
                        ),
                 args = args
            )                                                     
     output_dict["filtered_normalized_matrix"] = \
            add_pnas(
                 data = add_in_syngo(
                           data = add_lysoendo(
                                     data = add_gene_column(
                                                data_df_norm_filt_with_filt_da_da), 
                                     args = args
                                  ),
                           syngo_protein_cc_df = syngo_protein_cc_df,
                           args = args
                        ),
                 args = args
            )
     if args.ontology_annotate != "false":
          output_dict["filtered_ontology_annotation"] = enrich_filter_df
if args.filter_na != "false":
     output_dict["filtered_matrix_na_filt"] = \
            add_pnas(
                 data = add_in_syngo(
                          data = add_lysoendo(
                                     data = add_gene_column(
                                                data_df_filt_with_filt_da_da.loc[data_df_filt_with_filt_da_da.index.intersection(na_ind)]), 
                                     args = args
                                  ),
                          syngo_protein_cc_df = syngo_protein_cc_df,
                          args = args
                        ),
                 args = args
            )
     output_dict["filtered_normalized_matrix_na_filt"] = \
            add_pnas(
                 data = add_in_syngo(
                          data = add_lysoendo(
                                     data = add_gene_column(
                                               data_df_norm_filt_with_filt_da_da.loc[data_df_norm_filt_with_filt_da_da.index.intersection(na_ind)]), 
                                     args = args
                                  ),
                          syngo_protein_cc_df = syngo_protein_cc_df,
                          args = args
                        ),
                 args = args
            ) 
if args.diff_abundance and args.diff_abundance != "false":
    output_dict["diff_abundance_groups"] = da_group_df
    da_res_orig_with_filt_da = pd.merge(da_res_orig[da_result_fields].add_suffix("_prox"), filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True)
    output_dict["diff_abundance"] = add_gene_column(da_res_orig_with_filt_da)
da_data_df_filt_with_da = da_data_df_filt[all_samples]
if args.filter_pord != "false" or args.filter_pmod != "false" or args.filter_logfc != "false":
    da_data_df_filt_with_da = pd.merge(da_data_df_filt_with_da, filt_da_res_orig[da_result_fields].add_suffix("_spec"), left_index=True, right_index=True)
if args.diff_abundance and args.diff_abundance != "false":
    da_data_df_filt_with_da = pd.merge(da_data_df_filt_with_da, da_res_orig[da_result_fields].add_suffix("_prox"), left_index=True, right_index=True)
output_dict["filtered_matrix_after_da"] = \
       add_pnas(
           data = add_in_syngo(
                      data = add_gene_column(
                                 da_data_df_filt_with_da), 
                      syngo_protein_cc_df = syngo_protein_cc_df,
                      args = args
                  ),
           args = args
       )       

output_dict["da_ontology_annotation"] = enrich_da_df

if args.pca and args.pca != "false":
    output_dict["pca"] = pca_df
    if args.diff_abundance and args.diff_abundance != "false":
        output_dict["pca_filtered"] = pca_df_filt
if args.syngo_annotate:
    output_dict["ontology_annotation_plot"] = annot_df

with pd.ExcelWriter(os.path.join(args.output_folder,"results_summary.xlsx")) as writer:
   workbook = writer.book
   for key in output_dict.keys():
       output_dict[key].to_excel(writer, sheet_name=key)
       