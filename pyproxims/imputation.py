import pandas as pd
import numpy as np
import math
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import KNNImputer, IterativeImputer

def imputation(data_df, method, log_transform = True, norm_mean_q=0.05, norm_std_q=0.1, n_neighbors = 5, max_iter = 10):
    num_df = data_df.select_dtypes(include=np.number)
    if method == 'zero':
       res_df =  imputation_zero(num_df)
    elif method == 'norm':
       res_df = imputation_norm(num_df, log_transform, norm_mean_q, norm_std_q)
    elif method == 'lod':
       res_df = imputation_lod(num_df)
    elif method == 'knn':
       res_df = imputation_knn(num_df, n_neighbors)
    elif method == 'multivar':
       res_df = imputation_multivariate(num_df, max_iter)
    else:
       raise ValueError(f'imputation method {method} is invalid')
    res_df_full = data_df.copy()
    res_df_full[res_df.columns] = res_df
    return res_df_full
    
def imputation_zero(data_df):
    return data_df.fillna(0)
    
def impute_value_norm(x, std, quantile, log_transform, rng):
     cur_rnd = rng.normal(quantile, std)
     if log_transform:
        return 2**cur_rnd
     else:
        return abs(cur_rnd)
        
def impute_column_norm(col, stds, quantiles, log_transform, rng):
    return col.map(lambda x: impute_value_norm(x, stds[col.name], quantiles[col.name], log_transform, rng) if x==0 or np.isnan(x) else x)    

def imputation_norm(data_df, log_transform, norm_mean_q, norm_std_q):
    rng = np.random.default_rng(12345)
    if log_transform:
         logdata = data_df.applymap(lambda x: math.log2(x) if x!=0 and x!=np.nan else np.nan)
    else:
         logdata = data_df
    stds = {col: logdata[logdata[col].le(logdata[col].quantile(q=norm_std_q))][col].std() for col in logdata.columns}
    for col_name in stds:
       if math.isnan(stds[col_name]):
           stds[col_name] = 0
    quantiles = logdata.quantile(q=norm_mean_q)
    return data_df.apply(impute_column_norm, stds = stds, quantiles = quantiles, log_transform = log_transform, rng = rng)
    
def imputation_lod(data_df):
    return data_df.fillna(data_df.min())

def imputation_knn(data_df, n_neighbors=5):
    imputer = KNNImputer(n_neighbors=n_neighbors)
    return pd.DataFrame(imputer.fit_transform(data_df),columns = data_df.columns, index = data_df.index)
    
def imputation_multivariate(data_df, max_iter = 10):
    imputer = IterativeImputer(max_iter=max_iter, random_state=0)
    return pd.DataFrame(imputer.fit_transform(data_df),columns = data_df.columns, index = data_df.index)
