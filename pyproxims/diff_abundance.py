import rpy2.robjects as ro
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri,vectors, Formula, globalenv, r
pandas2ri.activate()

import pandas as pd
import numpy as np
import math
import plotly.graph_objects as go
import os

import matplotlib.pyplot as plt

limma = importr('limma')
stats = importr('stats')
qvalue = importr('qvalue')

r['source'](os.path.dirname(os.path.realpath(__file__))+"/source.functions.r")

def diff_abundance(data_df, group1, group2, log_transform = False):

    gr1gr2 = group1 + group2
    gr1gr2_ind = ['1' if x in group1 else '2' for x in gr1gr2]
    cha_df = pd.DataFrame(gr1gr2_ind, columns=['data'], dtype="category")
    design = stats.model_matrix(Formula('~data'), data=cha_df)
    eb_fit = globalenv['eb.fit']
    data_df_filt = data_df[gr1gr2]
    print(data_df_filt)
    print(design)
    if log_transform:
        data_df_filt = data_df_filt.applymap(lambda x: math.log2(x))
    res_eb = eb_fit(data_df_filt, design)
    with (ro.default_converter + pandas2ri.converter).context():
       res_eb_df = ro.conversion.get_conversion().rpy2py(res_eb)
    print(res_eb_df)
#    sys.exit()
    res_eb_df["logp.ord"] = res_eb_df["p.ord"].map(lambda x: -math.log10(x))
    res_eb_df["logp.mod"] = res_eb_df["p.mod"].map(lambda x: -math.log10(x))
    return res_eb_df

def volcano_plot(res_eb, p_axis, logfc_axis, p_thr = 0.5, logFC_thr = 0.5, protein_map = None):

    tmp_df = res_eb[[p_axis, logfc_axis]].copy()
    if protein_map is not None:
       tmp_df.index = tmp_df.index.map(lambda x: protein_map[x] if x in protein_map else x)
    
    max_logfc = tmp_df[logfc_axis].abs().max()*1.1
    tmp_df["logp.ord"] = tmp_df[p_axis].map(lambda x: -math.log10(x))
    tmp_df_interest = tmp_df[tmp_df[logfc_axis].ge(logFC_thr) & tmp_df["logp.ord"].ge(p_thr)]
    tmp_df_not_interest = tmp_df[~tmp_df.index.isin(list(tmp_df_interest.index))]

    fig1 = go.Figure()
    trace1 = go.Scatter(
     x=tmp_df_interest[logfc_axis],
     y=tmp_df_interest["logp.ord"],
     mode='markers+text',
     name='proteins of interest',
     text=list(tmp_df_interest.index),
     textposition="top center",
     line = dict(color='red'),
    )
    trace2 = go.Scatter(
     x=tmp_df_not_interest[logfc_axis],
     y=tmp_df_not_interest["logp.ord"],
     mode='markers',
     name='proteins',
     line = dict(color='blue'),
     text=list(tmp_df_interest.index),

    )

    fig1.update_traces(textposition='top center')
    fig1.update_xaxes(range=[-max_logfc, max_logfc], showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.update_yaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.add_trace(trace1)
    fig1.add_trace(trace2)

    fig1.update_layout(
        title="Differential abundance",
        xaxis_title="logFC",
        yaxis_title="-log10 p",
        legend_title="proteins",
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'
    )

    fig1.update_layout(
    title={
        'x': 0.5,
        'y': 0.95,
        'xanchor': 'center',
        'yanchor': 'top',
        'font':{
           'size': 30
        }})

    if p_thr is not None:
        fig1.add_hline(y=p_thr, line_width=1, line_dash="dash", line_color="black")
    if logFC_thr is not None:
        fig1.add_vline(x=logFC_thr, line_width=1, line_dash="dash", line_color="black")
#        fig1.add_vline(x=-logFC_thr, line_width=1, line_dash="dash", line_color="black")

    return fig1

def volcano_plot_matplotlib(res_eb, p_axis, logfc_axis, p_thr = 0.5, logFC_thr = 0.5, protein_map = None):

    tmp_df = res_eb[[p_axis, logfc_axis]].copy()
    if protein_map is not None:
       tmp_df.index = tmp_df.index.map(lambda x: protein_map[x] if x in protein_map else x)
    
    max_logfc = tmp_df[logfc_axis].abs().max()*1.1
    tmp_df["logp.ord"] = tmp_df[p_axis].map(lambda x: -math.log10(x))
    tmp_df_interest = tmp_df[tmp_df[logfc_axis].ge(logFC_thr) & tmp_df["logp.ord"].ge(p_thr)]
    tmp_df_not_interest = tmp_df[~tmp_df.index.isin(list(tmp_df_interest.index))]

    fig, ax = plt.subplots(nrows=1)
#    levels, categories = pd.factorize(res_pca[group_field])
#    color_map = {c: plt.cm.tab10(i) for i,c in enumerate(categories)}
#    colors = [plt.cm.tab10(i) for i in levels]
#    print(color_map)
#    print(colors)

#    for gr in categories:
#       cur_pca = res_pca[res_pca[group_field]==gr]
    sc1 = ax.scatter(x = tmp_df_interest[logfc_axis], y = tmp_df_interest["logp.ord"], c="red", label="proteins of interest")
    sc2 = ax.scatter(x = tmp_df_not_interest[logfc_axis], y = tmp_df_not_interest["logp.ord"], c="blue", label="proteins")

    ax.legend()
    num_filt = len(tmp_df_interest.index)
    fig.suptitle(f"Volcano ({num_filt} filtered)")
    return fig


    fig1 = go.Figure()
    trace1 = go.Scatter(
     x=tmp_df_interest[logfc_axis],
     y=tmp_df_interest["logp.ord"],
     mode='markers+text',
     name='proteins of interest',
     text=list(tmp_df_interest.index),
     textposition="top center",
     line = dict(color='red'),
    )
    trace2 = go.Scatter(
     x=tmp_df_not_interest[logfc_axis],
     y=tmp_df_not_interest["logp.ord"],
     mode='markers',
     name='proteins',
     line = dict(color='blue'),
     text=list(tmp_df_interest.index),

    )

    fig1.update_traces(textposition='top center')
    fig1.update_xaxes(range=[-max_logfc, max_logfc], showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.update_yaxes(showgrid=True, gridwidth=1, gridcolor='LightGrey', zeroline=True, zerolinewidth=1.5, zerolinecolor='Black')
    fig1.add_trace(trace1)
    fig1.add_trace(trace2)

    fig1.update_layout(
        title="Differential abundance",
        xaxis_title="logFC",
        yaxis_title="-log10 p",
        legend_title="proteins",
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor='rgba(0,0,0,0)'
    )

    fig1.update_layout(
    title={
        'x': 0.5,
        'y': 0.95,
        'xanchor': 'center',
        'yanchor': 'top',
        'font':{
           'size': 30
        }})

    if p_thr is not None:
        fig1.add_hline(y=p_thr, line_width=1, line_dash="dash", line_color="black")
    if logFC_thr is not None:
        fig1.add_vline(x=logFC_thr, line_width=1, line_dash="dash", line_color="black")
#        fig1.add_vline(x=-logFC_thr, line_width=1, line_dash="dash", line_color="black")

    return fig1


