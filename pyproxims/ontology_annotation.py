import gseapy as gp
import pandas as pd

def add_in_syngo(data, syngo_protein_cc_df, args):
    if args.syngo_annotate == "false":
        return data

    syngo_cc_map = {row["protein_id"]: row for index, row in syngo_protein_cc_df.iterrows()}

    data["in_syngo"] = data.index.map(lambda x: x in syngo_cc_map)
    data["orginal_syngo"] = data.index.map(lambda x: syngo_cc_map[x]["original_syngo"] if x in syngo_cc_map else None)
    data["orginal_syngo_protein"] = data.index.map(lambda x: syngo_cc_map[x]["original_syngo_protein"] if x in syngo_cc_map else None)
    return data

def calc_enrichment(protein_list, gene_sets, protein_map, sortby='Adjusted P-value', p_threshold = 0.05, organism="Mouse"):
    print(protein_list)
    gene_list = [protein_map[protein].split(" ") for protein in protein_list if protein in protein_map and isinstance(protein_map[protein],str)]   
    gene_list = list(set([gene for elem in gene_list for gene in elem]))
    print(gene_list)
    enr = gp.enrichr(gene_list = gene_list,
                     gene_sets = gene_sets,
                     organism = organism, 
                     outdir=None,
                     cutoff=0.5 # test dataset, use lower value from range(0,1)
                    )
    res = enr.results.sort_values(by=['Gene_set', sortby])
    categ_list = res['Gene_set'].unique()
    enrich_list = []
    for categ in categ_list:
        cur_df = res[res['Gene_set']==categ].sort_values(by=[sortby])
        cur_df = cur_df[cur_df['Adjusted P-value'].lt(p_threshold)]
        enrich_list.append(cur_df)
    res2 = pd.concat(enrich_list)
    return res2

def search_field(df, keywords, keywords_neg = None):
    for col in df.columns:
        if all([keyword.lower() in col.lower() for keyword in keywords]):
             if keywords_neg is not None:
                if all([keyword.lower() not in col.lower() for keyword in keywords_neg]):
                    return col
             else:
                return col 
    raise ValueError(f"column with keywords {keywords} is not found (available columns {df.columns})")

def create_syngo_cc_protein_map(syngo_protein_cc_df, uniprot_dict_map, ontology_full_parent_map):
    expr_protein_map = {}
    go_term_id_col = search_field(syngo_protein_cc_df,["go","term","ids"])
    go_term_name_col = search_field(syngo_protein_cc_df,["go","term","names"])
    protein_id_col = search_field(syngo_protein_cc_df,["protein"],["name"])
    is_presynaptic_col = search_field(syngo_protein_cc_df,["presynaptic"])
    is_postsynaptic_col = search_field(syngo_protein_cc_df,["postsynaptic"])
    if uniprot_dict_map is None:
        uniprot_dict_map = {}
    for index, row in syngo_protein_cc_df.iterrows():
        protein_id = row[protein_id_col]
        expr_protein_map[protein_id] = {}
        expr_protein_map[protein_id]["protein_id"] = protein_id
        expr_protein_map[protein_id]["go_term_ids"] = row[go_term_id_col].split(";")
        expr_protein_map[protein_id]["go_term_names"] = row[go_term_name_col].split(";")
        expr_protein_map[protein_id]['is_presynaptic'] = row[is_presynaptic_col]
        expr_protein_map[protein_id]['is_postsynaptic'] = row[is_postsynaptic_col]
        expr_protein_map[protein_id]['protein_name'] = uniprot_dict_map[protein_id] if protein_id in uniprot_dict_map else protein_id
    return expr_protein_map
    
def create_syngo_ontology_id_name_map(syngo_ontology_df):
    syngo_ontology_id_name_map = {}
    ontology_term_id_col = search_field(syngo_ontology_df,["ontology","term","id"])
    ontology_term_name_col = search_field(syngo_ontology_df,["ontology","term"],["id"])
    for index, row in syngo_ontology_df.iterrows():
        ontology_term_id = row[ontology_term_id_col]
        ontology_term = row[ontology_term_name_col]
        syngo_ontology_id_name_map[ontology_term_id] = ontology_term
    return syngo_ontology_id_name_map

def create_syngo_ontology_direct_parent_map(syngo_ontology_df):
    ontology_direct_parent_map = {}
    ontology_term_id_col = search_field(syngo_ontology_df,["ontology","term","id"])
    parent_term_col = search_field(syngo_ontology_df,["parent","term","id"])
    for index, row in syngo_ontology_df.iterrows():
        ontology_term_id = row[ontology_term_id_col]
        parent_term_id = row[parent_term_col]
        if parent_term_id:
            ontology_direct_parent_map[ontology_term_id] = parent_term_id
    return ontology_direct_parent_map

def create_syngo_ontology_full_parent_map(ontology_direct_parent_map):   
    ontology_full_parent_map = {}
    for term_id in ontology_direct_parent_map:
        cur_term = term_id
        ontology_full_parent_map[term_id] = []
        while cur_term in ontology_direct_parent_map:
            ontology_full_parent_map[term_id].append(cur_term)
            cur_term = ontology_direct_parent_map[cur_term]
        ontology_full_parent_map[term_id].append(cur_term)
    return ontology_full_parent_map

# GO:0098793 presynapse   
# GO:0098794 postsynapse    
    
def is_presynaptic_term(term_id, ontology_full_parent_map):
    # Synapse
    if term_id == "GO:0045202":
        return False
    return "GO:0098793" in ontology_full_parent_map[term_id]

def is_postsynaptic_term(term_id, ontology_full_parent_map):
    if term_id == "GO:0045202":
        return False
    return "GO:0098794" in ontology_full_parent_map[term_id]

# count a number of occurences for each term
def calc_goterms(expr_df, field, is_presynaptic, is_postsynaptic, syngo_protein_cc_df, syngo_ontology_df, protein_map = {}):
    
    ontology_direct_parent_map = create_syngo_ontology_direct_parent_map(syngo_ontology_df)
    ontology_full_parent_map = create_syngo_ontology_full_parent_map(ontology_direct_parent_map)
    syngo_ontology_id_name_map = create_syngo_ontology_id_name_map(syngo_ontology_df)   
    
    syngo_cc_protein_map = create_syngo_cc_protein_map(syngo_protein_cc_df, protein_map, ontology_full_parent_map)
    
    expr_goterm_map = {}
    proteins_goterm_map = {}
    proteins_total = 0
    proteins_synaptic = 0
    proteins_in_syngo = 0
    if field == "index":
       expr_field = expr_df.index
    else:
       expr_field = expr_df[field]
    for protein in expr_field:
        proteins_total += 1
        if protein in syngo_cc_protein_map:
            proteins_in_syngo += 1
            goterms_exclude = []
            for goterm in syngo_cc_protein_map[protein]["go_term_ids"]:
                if goterm in ontology_full_parent_map:
                    for goterm_excl in ontology_full_parent_map[goterm]:
                        if goterm_excl != goterm:
                            goterms_exclude.append(goterm_excl)                           
            if (is_presynaptic and syngo_cc_protein_map[protein]["is_presynaptic"]) or \
               (is_postsynaptic and not syngo_cc_protein_map[protein]["is_presynaptic"] and syngo_cc_protein_map[protein]["is_postsynaptic"]):
                proteins_synaptic += 1
                for goterm in syngo_cc_protein_map[protein]["go_term_ids"]:
                    if goterm in goterms_exclude:
                        continue
                    if is_presynaptic and not is_presynaptic_term(goterm, ontology_full_parent_map):
                        continue
                    goterm_name = syngo_ontology_id_name_map[goterm]
                    if goterm_name not in expr_goterm_map:
                        expr_goterm_map[goterm_name] = 0
                        proteins_goterm_map[goterm_name] = []
                    if protein in proteins_goterm_map[goterm_name]:
                        continue
                    expr_goterm_map[goterm_name] += 1
                    proteins_goterm_map[goterm_name].append(protein_map.get(protein, protein))
    return expr_goterm_map, proteins_goterm_map, proteins_total, proteins_in_syngo, proteins_synaptic
