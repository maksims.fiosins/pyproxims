import pandas as pd
import numpy as np
import re

def merge_replicates(data_df, replicate_suffix = "T\d+"):
    regex = replicate_suffix + "$"
    repl_df = data_df.filter(regex = regex)
    no_repl_df = data_df.loc[:,~data_df.columns.isin(repl_df.columns)]

#    repl_merged_df = repl_df.groupby(lambda x: "_".join(x.split("_")[:-1]), axis=1).mean()
    repl_merged_df = repl_df.groupby(lambda x: re.sub(regex, "", x), axis=1).mean()
    repl_merged_df = pd.concat([repl_merged_df,no_repl_df], axis = 1)
    return repl_merged_df

def normalize(data_df):
    data_df_filt = data_df.select_dtypes(include=np.number)
    norm_df = 10000*data_df_filt/data_df_filt.sum()
    return norm_df

