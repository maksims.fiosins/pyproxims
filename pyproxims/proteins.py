from bioservices import UniProt
import pandas as pd

def split(lst, n): 
    # looping till length lst
    for i in range(0, len(lst), n):  
        yield lst[i:i + n] 

def uniprot_res_to_df(uniprot_res):
    string_list = uniprot_res.split("\n")
    header = string_list[0].split("\t")
    vals = []
    for cur_str in string_list[1:]:
        cur_str_lst = cur_str.split("\t")
#        print(cur_str_lst)
        cur_map = {key: val for key, val in zip(header, cur_str_lst)}
#        print(cur_map)
        vals.append(cur_map)
#    print(vals)
    res_df = pd.DataFrame(vals)
    return res_df

def uniprot_query_ids(id_list):
    service = UniProt()
    chunks = split(id_list, 100)
#    print(chunks)
    all_dfs = [] 
    for chunk in chunks:
        query = " OR ".join(chunk)
        cur_res = service.search(query)
#        print(cur_res)
        cur_df = uniprot_res_to_df(cur_res)
        cur_df = cur_df[cur_df["Entry"].isin(id_list)]
        all_dfs.append(cur_df)
#        print(cur_res)
    res_df = pd.concat(all_dfs, ignore_index=True)
    res_df["Gene Names"] = res_df["Gene Names"].apply(lambda x: x.split(" ") if isinstance(x, str) else [])
    return res_df
