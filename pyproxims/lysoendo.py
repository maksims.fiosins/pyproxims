import pandas as pd

def add_lysoendo(data, args):
    global lyso_map
    global endo_map
    global endo_manual_map
    
    if args.lyso_annotate!="false":
        if lyso_map is None:
            load_lyso(args)
        lyso_map_len = max(data.index.map(lambda x: len(lyso_map[x]) if x in lyso_map else 0))
        if lyso_map_len>0:
            data["lysosome_enrichment"] = data.index.map(lambda x: lyso_map[x][0]["compartment"] if x in lyso_map else None)
    if args.endo_annotate!="false":
        if endo_map is None:
            load_endo(args)
        if endo_manual_map is None:
            load_endo_manual(args)
        endo_map_len = max(data.index.map(lambda x: len(endo_map[x]) if x in endo_map else 0))
        if endo_map_len>0:
            data["endosome_enrichment"] = data.index.map(lambda x: endo_map[x][0]["compartment"] if x in endo_map else None)
        endo_manual_map_len = max(data.index.map(lambda x: len(endo_manual_map[x]) if x in endo_manual_map else 0))
        if endo_manual_map_len>0:
            data["endosome_manual_enrichment"] = data.index.map(lambda x: endo_manual_map[x][0]["compartment"] if x in endo_manual_map else None)
    return data

        
def load_lyso(args):
    global lyso_map
    
    lyso_df = pd.read_excel(args.lyso_data, sheet_name = args.lyso_sheet)
    lyso_map = {}
    for index, row in lyso_df.iterrows():
        mouse_prot = row["Mouse Proteins"] 
        if not isinstance(mouse_prot, str):
            continue
        for prot in mouse_prot.split(","):
            if prot not in lyso_map:
                lyso_map[prot] = []
            lyso_map[prot].append({"human_gene": row["Revised Gene"], "compartment": row["Compartment"]})
    
def load_endo(args):
    global endo_map

    endo_df = pd.read_excel(args.endo_data, sheet_name = args.endo_sheet)
    endo_map = {}
    for index, row in endo_df.iterrows():
        mouse_prot = row["Mouse Proteins"] 
        if not isinstance(mouse_prot, str):
            continue
        for prot in mouse_prot.split(","):
            if prot not in endo_map:
                endo_map[prot] = []
            endo_map[prot].append({"human_gene": row["Revised Gene"], "compartment": row["Compartment (Itzhak 2016)"]})

    
def load_endo_manual(args):
    global endo_manual_map

    endo_manual_df = pd.read_excel(args.endo_data, sheet_name = args.endo_manual_sheet)
    endo_manual_map = {}
    for index, row in endo_manual_df.iterrows():
        mouse_prot = row["Mouse Proteins"] 
        if not isinstance(mouse_prot, str):
            continue
        for prot in mouse_prot.split(","):
            if prot not in endo_manual_map:
                endo_manual_map[prot] = []
            endo_manual_map[prot].append({"human_gene": row["Gene"], "compartment": row["Uniprot/Human protein atlas (HPA)"]})

lyso_map = None    
endo_map = None
endo_manual_map = None
